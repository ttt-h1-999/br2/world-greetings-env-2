
# You will notice that I have a propensity to use the bash container and do 
# runtime installs on it rather than prepare a container, this is because I find:
#  - It avoids unnecessary artifact storage and delays when chaining micro-single purpose containers
#  - It avoids the frustation of busybox shell with no package manager that is frequent in containers 
#    like skopeo, kaniko, crane, etc, etc (remember GitLab scripts run inside these containers, so 
#    their ultra minimalism causes you to have to use artifacts or cache to pass results between ultra-minimalized containers)
#  - It avoids container loading delays - especially on SaaS runners where for security reasons the pull 
#    container from source registry policy is set to "always" (which results in higher runner minutes 
#    consumption and longer jobs versus small runtime installs)
#  - It allows full, modern bash capabilities in shell scripts like regex in if statements and advanced variable expansions

stages:
  - vercheck
  - deploy

variables:
  #These variables can be manually forced in a pipeline run for testing purposes.
  NEXTVERSIONTOUSE: 'read-from-registry'
    #description: "Set this variable to a valid version tag in the monitored container registry to force a specific version (forces read-from-variable). 'read-from-registry' finds the latest version in the registry (latest-prod tag). 'read-from-file' uses the version stored in a file called NEXTVERSIONTOUSE so MRs can be used to update the version."
    #value: 'read-from-registry'    
  CD_STRATEGY: 'deliver-to-staging'
    #description: "Set to 'deliver-to-staging' for continuous delivery to staging and required approval to production. Set to 'deploy-to-production' for continous deployment to production without approvals."
    #value: 'deliver-to-staging'
  SERVICE_NAME: "hello-world" 
  IMAGE_NAME_TO_MONITOR: "registry.gitlab.com/ttt-h1-999/br2/hello-world/mainn"
  SEMVERREGEX: '^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)$'
  #CI_DEBUG_TRACE: "true" 

check-and-update-version:
  stage: vercheck
  image: bash
  script:
    - |
      # The bash variable expansion '${CD_STRATEGY##*-}' parses the last segment from $CD_STRATEGY which contains the first environment to deploy to.
      
      function validate-semversion () {
        if [[ -z "${1%%+( )}" ]]; then
          echo "The value of NEXTVERSIONTOUSE is '$NEXTVERSIONTOUSE' (blank), which is not valid, erroring out..."
          exit 5
        elif ! [[ "${1%%+( )}" =~ ${SEMVERREGEX} ]]; then
          echo "**** WARNING: '${1}' is not a simple semantec version (no prereleases allowed for 'simple'). If this is intentional, it is OK."
        fi
      }
      #manual install because yq alpline packaged version is old and does not work with envar substition in yq select
      
      wget -O - https://github.com/mikefarah/yq/releases/download/v4.25.1/yq_linux_amd64 -O /usr/bin/yq
      chmod +x /usr/bin/yq 
      MANIFESTVERSION=$( yq eval '.spec.template.spec.containers[].image | select(. == "${IMAGE_NAME_TO_MONITOR}*" |= envsubst)' manifests/${SERVICE_NAME}.${CD_STRATEGY##*-}.yaml | cut -d ":" -f 2)
      echo "Pulling version from '${CD_STRATEGY##*-}' environment manifest file manifests/${SERVICE_NAME}.${CD_STRATEGY##*-}.yaml"
      echo "Manifest version of '${CD_STRATEGY##*-}' environment is '$MANIFESTVERSION'"

      echo "NOTE: If source code has changed, the deploy child pipeline will always run regardless of version changes (in order to account for package manifest and CI changes)."
      echo "Determing how NEXTVERSION has been provided..."
      if [[ "${NEXTVERSIONTOUSE,,}" == "read-from-file" ]]; then
        echo "NEXTVERSIONTOUSE is set to ${NEXTVERSIONTOUSE,,}, attempting to read from file..."
        export NEXTVERSION=$(<NEXTVERSIONTOUSE)
        echo "Found version '$NEXTVERSION' in file called NEXTVERSIONTOUSE, using it..."
        COMMIT_MESSAGE="$NEXTVERSION (Version Source NEXTVERSIONTOUSEFILE), childOf:${CI_COMMIT_SHA} done in jobUrl:${CI_JOB_URL}"
        validate-semversion $NEXTVERSION
      elif [[ "${NEXTVERSIONTOUSE,,}" == "read-from-registry"  ]]; then
        echo "NEXTVERSIONTOUSE is set to '${NEXTVERSIONTOUSE,,}', attempting to read version from container registry..."
        apk add --quiet jq skopeo
        echo "IMAGE_NAME_TO_MONITOR: ${IMAGE_NAME_TO_MONITOR}"
        skopeo login --username ${READ_REG_USER} --password ${READ_REG_TOKEN} ${CI_REGISTRY}
        export NEXTVERSION=$(skopeo inspect  docker://$IMAGE_NAME_TO_MONITOR:latest-prod | jq -r '.Labels["org.opencontainers.image.version"]')
        echo "Found version '$NEXTVERSION' in registry image '$IMAGE_NAME_TO_MONITOR:latest-prod', using it..."
        COMMIT_MESSAGE="$NEXTVERSION (Version Source latest-prod image tag), childOf:${CI_COMMIT_SHA} done in jobUrl:${CI_JOB_URL}"
        validate-semversion $NEXTVERSION
      else
        echo "NEXTVERSIONTOUSE was specified as '$NEXTVERSIONTOUSE', using it directly..."
        export NEXTVERSION=${NEXTVERSIONTOUSE}
        COMMIT_MESSAGE="$NEXTVERSION (Version Source - NEXTVERSIONTOUSE variable during pipeline triggering), childOf:${CI_COMMIT_SHA} done in jobUrl:${CI_JOB_URL}"
        validate-semversion $NEXTVERSION
      fi

      echo "Determining if we have a version mismatch"
      if [[ "${MANIFESTVERSION}" != "${NEXTVERSION}" ]]; then
        echo "The ${CD_STRATEGY##*-} environment is at version ${MANIFESTVERSION}, updating it to version ${NEXTVERSION}"
        echo "Passing variables into pipeline using .env artifact file..."
        echo "VERSIONUPDATE=true" | tee -a version.env
        echo "NEXTVERSION=${NEXTVERSION}" | tee -a version.env
      else
        echo "The ${CD_STRATEGY##*-} environment version (${MANIFESTVERSION}) already matches the requested version (${NEXTVERSION}) no udpates needed due to version number changes."
        echo "NOTE: source file changes will still trigger pipeline in case of package manifest changes or CI changes."
        echo "Passing variables into pipeline using .env artifact file..."
        echo "VERSIONUPDATE=false" | tee -a version.env
        echo "NEXTVERSION=${MANIFESTVERSION}" | tee -a version.env #still pass the next version in case we are running due to packaged manifest changes
      fi
      
      # Forward push pipeline source information to child pipeline
      GIT_PUSH_OCCURRED='false'
      if [[ "$CI_PIPELINE_SOURCE" == "push" ]]; then
        GIT_PUSH_OCCURRED='true'
      fi
      echo "GIT_PUSH_OCCURRED=${GIT_PUSH_OCCURRED}" | tee -a version.env
      echo "COMMIT_MESSAGE=$NEXTVERSION (Deployed due to being found in version tag of latest-prod image)" | tee -a version.env

  artifacts:
    reports:
      dotenv: version.env

deploy:
  stage: deploy
  variables:
    VERSIONUPDATE: $VERSIONUPDATE
    NEXTVERSION: $NEXTVERSION
    GIT_PUSH_OCCURRED: $GIT_PUSH_OCCURRED
    COMMIT_MESSAGE: $COMMIT_MESSAGE
  trigger:
    include: update-manifests.gitlab-ci.yml  
  allow_failure: true
